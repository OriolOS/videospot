//
//  ProfileViewController.m
//  VideoSpot
//
//  Created by Winparf on 15/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "ProfileViewController.h"
#import "UserServiceManager.h"
#import "VideoPostServiceManager.h"
#import "FollowersServiceManager.h"
#import "FollowersViewController.h"

@interface ProfileViewController () <UserServiceManagerDelegate, VideoPostServiceManagerDelegate, FollowersServiceManagerDelegate>
@property (nonatomic,strong) UserServiceManager *userManager;
@property (nonatomic,strong) VideoPostServiceManager *videoPostServiceManager;
@property (nonatomic,strong) FollowersServiceManager *followersServiceManager;

@property (nonatomic, strong) NSMutableArray *followersArray;
@property (nonatomic, strong) NSMutableArray *followingArray;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.videoPostServiceManager = [[VideoPostServiceManager alloc]init];
    self.videoPostServiceManager.delegate = self;
    
    self.followersArray = [[NSMutableArray alloc]init];
    self.followingArray = [[NSMutableArray alloc]init];
    
    self.followersServiceManager = [[FollowersServiceManager alloc]init];
    self.followersServiceManager.delegate = self;

    
    [self updateView];
    
    self.followersButton.layer.cornerRadius = 10;
    self.logOutButton.layer.cornerRadius = 10;
    
    self.imagePicker = [[UIImagePickerController alloc]init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = YES;
 
    
    self.imageView.alpha = 1;
    self.imageView.layer.cornerRadius = 125;
    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.userManager = [[UserServiceManager alloc]init];
    self.userManager.delegate = self;

}

-(void)viewWillAppear:(BOOL)animated{
    [self.videoPostServiceManager getCountOfVideoPost];
    [self.followersServiceManager getFollowersForUser];
}

-(void)updateView{
    NSString *imageUrl =[[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]objectForKey:@"ProfileImage"];
    if (imageUrl.length) {
        self.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]]];
    }
    
    NSString *username = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]objectForKey:@"username"];
    if (username.length) {
        self.usernameLabel.text = username;
    }
    
    NSString *email = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]objectForKey:@"email"];
    if (username.length) {
        self.emailLabel.text = email;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)followersButtonPressed:(id)sender {
    
    UINavigationController *nc = [self.storyboard instantiateViewControllerWithIdentifier:@"followersNC"];
    
    FollowersViewController *followersVC = (FollowersViewController *)nc.topViewController;
    followersVC.followersArray = [[NSMutableArray alloc]initWithArray:self.followersArray];
    followersVC.followingArray = [[NSMutableArray alloc]initWithArray:self.followingArray];
    
    [self presentViewController:nc animated:YES completion:nil];
    
}
- (IBAction)logOutButtonPressed:(id)sender {

    [PFUser logOut];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UserApp"];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)getPhotoProfileButtonPressed:(id)sender {
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - FollowersServiceManager Delegate

-(void)didReceivedFollowers:(NSArray *)followersArray{
    [self.followersArray removeAllObjects];
    for (PFObject *object in followersArray) {
        [self.followersArray addObject:object[@"Follower"]];
    }
    
    self.followersLabel.text = [NSString stringWithFormat:@"%lu followers", (unsigned long)self.followersArray.count];
    
    [self.followersServiceManager getFollowingForUser];
}

-(void)didReceivedFollowing:(NSArray *)followingArray{
    [self.followingArray removeAllObjects];
    for (PFObject *object in followingArray) {
        [self.followingArray addObject:object[@"Following"]];
    }
    self.followingLabel.text = [NSString stringWithFormat:@"%lu following", (unsigned long)self.followingArray.count];
}

#pragma mark - UserServiceManager Delegate

-(void)didSetImage:(NSString *)imageUrl{
    
    if (imageUrl) {
        [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]setObject:imageUrl forKey:@"ProfileImage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self updateView];
}

#pragma mark - VideoPostServiceManager Delegate

-(void)didGetNumberOfPost:(int)count{
   
    if (!count) {
        count = 0;
    }
    self.postLabel.text = [NSString stringWithFormat:@"%d post",count];
}


#pragma mark UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [self.userManager setImageProfile:chosenImage];
    
    self.imageView.alpha = 1;
    self.imageView.image = chosenImage;
    self.imageView.layer.cornerRadius = 125;
    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


@end
