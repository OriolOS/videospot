//
//  SecondViewController.m
//  VideoSpot
//
//  Created by Winparf on 15/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "SecondViewController.h"
#import <ParseUI/ParseUI.h>
#import "VideoTableViewCell.h"
#import "AdvertisementTableViewCell.h"

@interface SecondViewController () <UITableViewDataSource, UITableViewDelegate, VideoPostServiceManagerDelegate, FollowersServiceManagerDelegate, UserServiceManagerDelegate>

@property(nonatomic,strong) UIRefreshControl *refreshControl;
@property(nonatomic,strong) NSString *filterPost;
@property(nonatomic) NSInteger skip;
@property(nonatomic) NSInteger lastRowDisplayed;


@property(nonatomic,strong) NSMutableArray *videoPosts;
@property(nonatomic,strong) NSMutableArray *followingArray;
@property (nonatomic,strong) NSMutableArray *followingUsers;

@property (nonatomic) int followingNumber;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.videoPosts = [NSMutableArray new];
    self.videoPostServiceManager = [[VideoPostServiceManager alloc]init];
    self.videoPostServiceManager.delegate = self;
    
    self.followingArray = [[NSMutableArray alloc]init];
    if (!_followingUsers) {
        _followingUsers = [[NSMutableArray alloc]init];
    }
    
    self.followersServiceManager = [[FollowersServiceManager alloc]init];
    self.followersServiceManager.delegate = self;
    
    self.userManager = [[UserServiceManager alloc]init];
    self.userManager.delegate = self;
    
    self.followingNumber = 0;
    
    self.skip = 0;
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor lightGrayColor];
    self.refreshControl.tintColor = [UIColor darkGrayColor];
    [self.refreshControl addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    self.refreshControl.attributedTitle = attributedTitle;
    
    [self.tableView addSubview:self.refreshControl];

}
-(void)viewWillAppear:(BOOL)animated{
    [self.followersServiceManager getFollowingForUser];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}

-(void)refreshTableView{
    
    [self.videoPosts removeAllObjects];
    self.skip = 0;
    [self.videoPostServiceManager getVideoPostWithFilter:@"" WithSkipOf:0];
}

-(void) playButtonPressed:(id)sender{
    
    UIButton *playButton = (UIButton *)sender;
    VideoPost *videoPost = [self.videoPosts objectAtIndex:playButton.tag];
    
    // save it to the documents directory
    NSURL *fileURL = [self grabFileURL:@"tmp.mov"];
    [videoPost.videoData writeToURL:fileURL atomically:YES];
    
    // save it to the Camera Roll
    UISaveVideoAtPathToSavedPhotosAlbum([NSString stringWithFormat:@"%@",fileURL], nil, nil, nil);
    
    PlayerViewController *playerVC = [[PlayerViewController alloc]initWithContentURL:fileURL];
    [self presentMoviePlayerViewControllerAnimated:playerVC];
    
}

- (NSURL*)grabFileURL:(NSString *)fileName {
    
    // find Documents directory
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    // append a file name to it
    documentsURL = [documentsURL URLByAppendingPathComponent:fileName];
    
    return documentsURL;
}

-(NSString *)getTimeAgoForPostDate:(NSDate *)date{
    NSString *dateStr = @"";
    
    // get current date/time
    NSDate *today = [NSDate date];
    
    NSTimeInterval interval = [today timeIntervalSinceDate:date];
    
    NSNumber *intervalNumber = [NSNumber numberWithDouble:interval];
    float sec = [intervalNumber floatValue];
    
    float hour = sec/3600;
    int hourInt = hour;
    
    float minutes = sec/60 - hourInt*60;
    int minutesInt = minutes;
    
    int secInt = sec - hourInt*3600 - minutesInt*60;
    
    if (hour >= 24) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-mm-yyyy"];
        //Optionally for time zone conversions
        //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
        dateStr = [formatter stringFromDate:date];
    }else if (hourInt != 0 ){
        if (minutesInt != 0) {
            dateStr = [NSString stringWithFormat:@"%d h %d min ago",hourInt,minutesInt];
        }else{
            dateStr = [NSString stringWithFormat:@"%d h ago",hourInt];
        }
    }else if (minutesInt != 0){
        if (secInt != 0) {
            dateStr = [NSString stringWithFormat:@"%d min %d sec ago",minutesInt,secInt];
        }else{
            dateStr = [NSString stringWithFormat:@"%d min ago",minutesInt];
        }
    }else if (secInt >= 5){
        dateStr = [NSString stringWithFormat:@"%d sec ago",secInt];
    }else{
        dateStr = [NSString stringWithFormat:@"Now"];
    }
    
    return dateStr;
}

#pragma mark - UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.videoPosts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VideoTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"videoCell"];
    
    if (self.videoPosts.count) {
        VideoPost *videoPost = [self.videoPosts objectAtIndex:indexPath.row];
        
        cell.videoData = videoPost.videoData;
        
        cell.videoImageView.image = videoPost.previewVideoImage;
        cell.videoImageView.layer.cornerRadius = 10;
        cell.videoImageView.layer.masksToBounds = YES;
        
        cell.playButton.contentMode = UIViewContentModeScaleAspectFill;
        [cell.playButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.playButton setTag:indexPath.row];
        cell.playButton.layer.cornerRadius = 25;
        cell.playButton.layer.masksToBounds = YES;
        
        cell.userImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:videoPost.userImageUrl]]];
        cell.userImageView.layer.cornerRadius = 30;
        cell.userImageView.layer.masksToBounds = YES;
        
        
        [cell.usernameButton setTitle:videoPost.username forState:UIControlStateNormal];
        cell.usernameButton.enabled = NO;
        
        cell.descriptionLabel.text = videoPost.descriptionVideo;
        cell.descriptionLabel.layer.cornerRadius = 10;
        cell.descriptionLabel.layer.masksToBounds = YES;
        
        NSString *tags = @"";
        if (videoPost.tagsArray.count) {
            for (NSString *tag in videoPost.tagsArray) {
                tags = [NSString stringWithFormat:@"%@%@",tags,tag];
            }
        }
        cell.tagsLabel.text = tags;
        
        cell.timeAgoLabel.text = [self getTimeAgoForPostDate:videoPost.createDate];
        
        return cell;
        
    }else{
        [self refreshTableView];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 200;
}

#pragma mark -UITableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 5 && self.skip != 0) {
        [self.videoPostServiceManager getVideoPostWithFilter:@"" WithSkipOf:self.skip];
    }else if (indexPath.row == self.skip - 5 && (self.skip%10 - ((int)self.skip%10)) == 0){
        [self.videoPostServiceManager getVideoPostWithFilter:@"" WithSkipOf:self.skip];
    }
}


#pragma mark - VideoPostServiceManager Delegate

-(void)didGetVideoPost:(NSArray *)videoPosts{
    if (videoPosts.count == 10) {
        self.skip += 10;
    }else{
        self.skip = 0;
    }
    
    for (PFObject *object in videoPosts) {
        
        PFFile * file;
        
        VideoPost *videoPost = [VideoPost new];
        videoPost.tagsArray = [[NSMutableArray alloc]init];
        videoPost.descriptionVideo = object[@"Description"];
        [videoPost.tagsArray addObjectsFromArray:object[@"Tags"]];
        
        videoPost.username = object[@"Owner"];
        
        file = object[@"OwnerImage"];
        videoPost.userImageUrl = file.url;
        
        file = object[@"PreviewImage"];
        videoPost.previewVideoImage = [UIImage imageWithData:[file getData]];
        
        file = object[@"Video"];
        videoPost.videoData = [file getData];
        
        videoPost.createDate = object.createdAt;
        
        [self.videoPosts addObject:videoPost];
        
    }
    
    [self.tableView reloadData];
    
    [self.refreshControl endRefreshing];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm:ss a"];
    NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    self.refreshControl.attributedTitle = attributedTitle;
}

#pragma mark - FollowersServiceManager Delegate

-(void)didReceivedFollowing:(NSArray *)followingArray{
    
    [self.followingArray removeAllObjects];
    for (PFObject *object in followingArray) {
        [self.followingArray addObject:object[@"Following"]];
    }
    
    if (self.followingArray.count) {
        [self.followersServiceManager getUserWithObjectId:[self.followingArray objectAtIndex:self.followingNumber]];
    }else{
        [self.videoPostServiceManager getVideoPostWithFollowingArray:nil WithSkipOf:self.skip];
    }
    
}

-(void)didReceivedUserByObjectId:(PFUser *)user{
    if (user) {
        [self.followingUsers addObject:user.username];
        self.followingNumber ++;
        
        if (self.followingNumber < self.followingArray.count) {
            [self.followersServiceManager getUserWithObjectId:[self.followingArray objectAtIndex:self.followingNumber]];
        }else{
            [self.videoPostServiceManager getVideoPostWithFollowingArray:self.followingUsers WithSkipOf:self.skip];
        }
    }
}




@end
