//
//  SideMenuViewController.m
//  WinParfMobile
//
//  Created by Winparf on 26/3/15.
//  Copyright (c) 2015 WinParfServices. All rights reserved.
//

#import "SideMenuViewController.h"

@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) awakeFromNib
{
    [self setLeftPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"profileVC"]];
    [self setCenterPanel:[self.storyboard instantiateViewControllerWithIdentifier:@"tabBar"]];
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}

@end
