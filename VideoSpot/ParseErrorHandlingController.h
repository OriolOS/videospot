//
//  ParseErrorHandlingController.h
//  LocationTest
//
//  Created by Winparf on 17/6/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface ParseErrorHandlingController : NSObject

@property(nonatomic,strong) PFLogInViewController *logInViewController;
@property(nonatomic,strong) PFLogInView *logInView;
@property(nonatomic,strong) PFSignUpViewController *signUpViewController;

+ (void)handleParseError:(NSError *)error;


@end
