//
//  main.m
//  VideoSpot
//
//  Created by Winparf on 15/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
