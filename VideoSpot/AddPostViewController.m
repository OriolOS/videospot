//
//  AddPostViewController.m
//  VideoSpot
//
//  Created by Winparf on 16/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "AddPostViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>

@interface AddPostViewController () <UITextFieldDelegate, UITextViewDelegate,VideoPostServiceManagerDelegate>
@property (nonatomic,strong) UIImagePickerController *videoPicker;
@property (nonatomic,strong) NSString *urlVideo;
@property (nonatomic,strong) NSArray *tagsArray;
@end

@implementation AddPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tagsArray = [[NSMutableArray alloc]init];
    self.videoPostServiceManager = [[VideoPostServiceManager alloc]init];
    self.videoPostServiceManager.delegate = self;
    self.currentVideoPost = [[VideoPost alloc]init];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [self.videoAlbumButton addTarget:self action:@selector(videoAlbumButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    self.videoView.alpha = 0.3;
    self.videoView.layer.cornerRadius = 40;
    
    self.playButton.hidden = YES;
    [self.playButton addTarget:self action:@selector(playButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    self.playButton.layer.cornerRadius = 40;
    self.playButton.layer.masksToBounds = YES;
    
    self.descriptionTextView.delegate = self;
    self.tagsTextField.delegate = self;
    
    self.videoAlbumButton.layer.cornerRadius = 10;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)doneButtonPressed:(id)sender {
    if (self.videoView.image) {
        self.currentVideoPost.previewVideoImage = self.videoView.image;
        self.currentVideoPost.urlVideo = self.urlVideo;
        NSString *userImageUrl = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]objectForKey:@"ProfileImage"];
        if (userImageUrl.length) {
            self.currentVideoPost.userImageUrl = userImageUrl;
        }
        self.currentVideoPost.username = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]objectForKey:@"username"];
        self.currentVideoPost.descriptionVideo = self.descriptionTextView.text;
        [self setTagsArray];
        self.currentVideoPost.tagsArray = [NSMutableArray new];
        [self.currentVideoPost.tagsArray addObjectsFromArray:self.tagsArray];
        [self.videoPostServiceManager saveVideoPost:self.currentVideoPost];
        
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"There is no video to upload"
                                                              message:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
}

-(void)setTagsArray{
    NSString *tags;
    if (self.tagsTextField.text) {
        tags = self.tagsTextField.text;
    }
    self.tagsArray = [tags componentsSeparatedByString:@" "];
}

- (void)videoAlbumButtonPressed {
    
    self.videoPicker = [[UIImagePickerController alloc]init];
    self.videoPicker.delegate = self;
    self.videoPicker.allowsEditing = YES;
    self.videoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.videoPicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeVideo, nil];
    NSArray *sourceTypes = [UIImagePickerController availableMediaTypesForSourceType:self.videoPicker.sourceType];
    if ([sourceTypes containsObject:@"kUTTypeMovie" ]){
        //NSLog(@"no video");
    }
    [self presentViewController:self.videoPicker animated:YES completion:nil];
}
-(void) didGetVideoFromAlbumWithDictionary:(NSDictionary *)videoDict{
    
    float sizeVideo = [[videoDict objectForKey:@"videoSize"] floatValue];
    
    if (sizeVideo > 5) {
        //alertContgroller
    }else{
        self.currentVideoPost.urlVideo = [videoDict objectForKey:@"urlVideo"];
        self.currentVideoPost.previewVideoImage = [videoDict objectForKey:@"previewVideoImage"];
        
        self.videoView.image = self.currentVideoPost.previewVideoImage;
        self.videoView.contentMode = UIViewContentModeScaleToFill;
        self.videoView.alpha = 1;
        self.videoView.layer.cornerRadius = 20;
        self.playButton.hidden = NO;
        
        [self.view bringSubviewToFront:self.playButton];
        
    }

    
}
-(void) playButtonPressed{
    
    PlayerViewController *playerVC = [[PlayerViewController alloc]
                                      initWithContentURL:[NSURL URLWithString:self.urlVideo]];
    
    [self.navigationController presentMoviePlayerViewControllerAnimated:playerVC];
    
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - VideoPostServiceManafer Delegate

-(void)didSavedVideoPost:(BOOL)succeeded{
    if (succeeded) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Post has not been saved"
                                                              message:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"TRY AGAIN"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
}

#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSMutableDictionary *videoDict = [NSMutableDictionary new];
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    NSLog(@"type=%@",type);
    if ([type isEqualToString:(NSString *)kUTTypeImage]){

        
    }else{
        self.urlVideo = [NSString stringWithFormat:@"%@",[info objectForKey:UIImagePickerControllerMediaURL]];
        
        AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:self.urlVideo]];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        CMTime time = [asset duration];
        time.value = 0;
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
        
        [videoDict setValue:self.urlVideo forKey:@"urlVideo"];
        [videoDict setValue:thumbnail forKey:@"previewVideoImage"];
        
        if(asset != nil){
            NSArray *tracks = [asset tracks];
            float estimatedSize = 0.0 ;
            for (AVAssetTrack * track in tracks) {
                float rate = ([track estimatedDataRate] / 8); // convert bits per second to bytes per second
                float seconds = CMTimeGetSeconds([track timeRange].duration);
                estimatedSize += seconds * rate;
            }
            float sizeInMB = estimatedSize / 1024 / 1024;
            NSNumber *sizeVideoInMB = [[NSNumber alloc]initWithFloat:sizeInMB];
            [videoDict setValue:sizeVideoInMB forKey:@"videoSize"];
        }

    }
    [self.videoPicker dismissViewControllerAnimated:YES completion:nil];
    [self didGetVideoFromAlbumWithDictionary:videoDict];
}

#pragma mark UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField.text isEqualToString:@"#tags"]) {
        textField.text = @"";
    }
    if (!textField.text.length) {
        textField.text = [NSString stringWithFormat:@"#"];
    }
    if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
        self.view.frame = CGRectMake(0, self.view.frame.size.height/5 - textField.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }else{
        self.view.frame = CGRectMake(0, self.view.frame.size.height/2 - textField.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([string isEqualToString:@" "]) {
        
        [self.tagsTextField setText:[self.tagsTextField.text stringByReplacingCharactersInRange:range withString:@" #"]];
        return NO;
    }else{
        return YES;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyboard];
    return YES;
}



#pragma mark UITextView Delegate

-(void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Video description"]) {
        textView.text = @"";
    }
    
    if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
        self.view.frame = CGRectMake(0, self.view.frame.size.height/5 - textView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }else{
        self.view.frame = CGRectMake(0, self.view.frame.size.height/3- self.descriptionTextView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}



@end
