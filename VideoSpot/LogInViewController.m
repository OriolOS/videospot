//
//  LogInViewController.m
//  VideoSpot
//
//  Created by Winparf on 15/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <ParseUI/ParseUI.h>
#import "LogInViewController.h"
#import "SideMenuViewController.h"

@interface LogInViewController ()



@end

@implementation LogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

-(void)viewDidAppear:(BOOL)animated{
    SideMenuViewController *sideMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"sideMenu"];
    sideMenuVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:sideMenuVC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
