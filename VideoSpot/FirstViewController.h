//
//  FirstViewController.h
//  VideoSpot
//
//  Created by Winparf on 15/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "PlayerViewController.h"
#import "VideoPostServiceManager.h"
#import "UserServiceManager.h"
#import "FollowersServiceManager.h"

@interface FirstViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) VideoPostServiceManager *videoPostServiceManager;
@property (nonatomic,strong) UserServiceManager *userManager;
@property (nonatomic,strong) FollowersServiceManager *followersServiceManager;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;


@end

