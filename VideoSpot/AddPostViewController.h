//
//  AddPostViewController.h
//  VideoSpot
//
//  Created by Winparf on 16/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "PlayerViewController.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "VideoPost.h"
#import <Parse/Parse.h>
#import "VideoPostServiceManager.h"


@interface AddPostViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) VideoPost *currentVideoPost;
@property (weak, nonatomic) IBOutlet UIImageView *videoView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *videoAlbumButton;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UITextField *tagsTextField;
@property (nonatomic, strong) VideoPostServiceManager *videoPostServiceManager;

@end
