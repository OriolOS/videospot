//
//  UserServiceManager.h
//  VideoSpot
//
//  Created by Winparf on 20/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//


#import <AVFoundation/AVFoundation.h>
#import <Parse/Parse.h>
#import "Reachability.h"

@protocol UserServiceManagerDelegate

//User methods delegate

-(void)didUpdateUser:(BOOL)succeeded;

-(void)didReceivedUserById:(PFObject *)user;

-(void)didReceivedUserByName:(PFObject *)user;

-(void)didSetImage:(NSString *)imageUrl;

//Reachability method delegate

-(void)hasNoConnection;


@end

@interface UserServiceManager : NSObject

@property (nonatomic,strong) id <UserServiceManagerDelegate> delegate;

//User methods

-(void)updateUserWithUsername:(NSString *)username Email:(NSString *)email;

-(void)setImageProfile:(UIImage *) image;

-(void)getUserWithId:(NSString *)objectId;

-(void)getUserWithUserName:(NSString *)username;


@end
