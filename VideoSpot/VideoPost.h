//
//  VideoPost.h
//  VideoSpot
//
//  Created by Winparf on 16/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface VideoPost : NSObject

@property (nonatomic, strong) NSData *videoData;
@property (nonatomic, strong) NSString *urlVideo;
@property (nonatomic, strong) UIImage *previewVideoImage;
@property (nonatomic, strong) NSString *userImageUrl;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *descriptionVideo;
@property (nonatomic, strong) NSMutableArray *tagsArray;
@property (nonatomic, strong) NSDate *createDate;

@end
