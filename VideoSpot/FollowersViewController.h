//
//  FollowersViewController.h
//  VideoSpot
//
//  Created by Winparf on 16/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowersViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *followersArray;
@property (nonatomic,strong) NSMutableArray *followingArray;

@end
