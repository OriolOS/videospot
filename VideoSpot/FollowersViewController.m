//
//  FollowersViewController.m
//  VideoSpot
//
//  Created by Winparf on 16/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "FollowersServiceManager.h"
#import "UserServiceManager.h"
#import "FollowersViewController.h"
#import "SWTableViewCell.h"

@interface FollowersViewController () < UITableViewDataSource, UITableViewDelegate, FollowersServiceManagerDelegate,SWTableViewCellDelegate>

@property (nonatomic,strong) FollowersServiceManager *followersServiceManager;

@property (nonatomic,strong) NSMutableArray *followingUsers;
@property (nonatomic,strong) NSMutableArray *followersUsers;

@property (nonatomic) int followingNumber;
@property (nonatomic) BOOL isFollowingUser;
@property (nonatomic) int followersNumber;
@property (nonatomic) BOOL isFollowerUser;

@end

@implementation FollowersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (!_followersUsers) {
        _followersUsers = [[NSMutableArray alloc]init];
    }
    if (!_followingUsers) {
        _followingUsers = [[NSMutableArray alloc]init];
    }


    self.followersServiceManager = [[FollowersServiceManager alloc]init];
    self.followersServiceManager.delegate = self;
    

    
    self.followingNumber = 0;
    self.followersNumber = 0;
}

-(void)viewWillAppear:(BOOL)animated{
    
    if (self.followingArray.count) {
        [self.followersServiceManager getUserWithObjectId:[self.followingArray objectAtIndex:self.followingNumber]];
        self.isFollowingUser = YES;
    }else if (self.followersArray.count){
        [self.followersServiceManager getUserWithObjectId:[self.followersArray objectAtIndex:self.followersNumber]];
        self.isFollowerUser = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"Following";
    }else{
        return @"Followers";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 0) {
        return self.followingArray.count;
    }else{
        return self.followersArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    SWTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"followerCell"];
    cell.delegate = self;

    
    if (indexPath.section == 0 && self.followingUsers.count) {

            PFUser *user = [self.followingUsers objectAtIndex:indexPath.row];
            
            cell.textLabel.text = user.username;
            cell.detailTextLabel.text = user.email;
            
            if (user[@"ProfileImage"]) {
                PFFile *imageFile = user[@"ProfileImage"];
                cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageFile.url]]];
                cell.imageView.layer.cornerRadius = 10;
                cell.imageView.layer.masksToBounds = YES;
            }
        cell.rightUtilityButtons = [self rightButtons];

    }else if (indexPath.section == 1 && self.followersUsers.count){
        PFUser *user = [self.followersUsers objectAtIndex:indexPath.row];
        
        cell.textLabel.text = user.username;
        cell.detailTextLabel.text = user.email;
        
        if (user[@"ProfileImage"]) {
            PFFile *imageFile = user[@"ProfileImage"];
            cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageFile.url]]];
            cell.imageView.layer.cornerRadius = 20;
            cell.imageView.layer.masksToBounds = YES;
        }
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 50;
}

#pragma mark SWTableviewCell

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
    
}
-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index{
    
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (NSArray *)rightButtons{
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] icon:[UIImage imageNamed:@"delete.png"]];
    return rightUtilityButtons;
}

- (NSArray *)leftButtons{
    
    return nil;
}


#pragma mark - FollowersServiceManager Delegate

-(void)didReceivedUserByObjectId:(PFObject *)user{
    
    if (user && self.isFollowingUser) {
        [self.followingUsers addObject:user];
        self.followingNumber ++;
        if (self.followingNumber < self.followingArray.count) {
            [self.followersServiceManager getUserWithObjectId:[self.followingArray objectAtIndex:self.followingNumber]];
        }
        
    }else if (!user && self.isFollowingUser && !self.isFollowerUser){
        self.followingNumber = 0;
        self.isFollowingUser = NO;
        self.isFollowerUser = YES;
        if (self.followersNumber < self.followersArray.count) {
            [self.followersServiceManager getUserWithObjectId:[self.followersArray objectAtIndex:self.followersNumber]];
        }
    }else if(user && self.isFollowerUser){
        [self.followersUsers addObject:user];
        self.followersNumber ++;
        if (self.followersNumber < self.followersArray.count) {
            [self.followersServiceManager getUserWithObjectId:[self.followersArray objectAtIndex:self.followersNumber]];
        }
        
    }
    
    [self.tableView reloadData];
}






@end
