//
//  FollowersServiceManager.h
//  VideoSpot
//
//  Created by Winparf on 22/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Parse/Parse.h>
#import "Reachability.h"

@protocol FollowersServiceManagerDelegate <NSObject>

-(void)didReceivedFollowers:(NSArray *)followersArray;

-(void)didReceivedFollowing:(NSArray *)followingArray;

-(void)didJustFollowUser:(BOOL)succeeded;

-(void)alreadyFollowing;

-(void)didReceivedUserByObjectId:(PFObject *)user;

//Reachability method delegate

-(void)hasNoConnection;

@end

@interface FollowersServiceManager : NSObject

@property (nonatomic,strong) id <FollowersServiceManagerDelegate> delegate;

-(void)followUser:(NSString *)objectId;

-(void)getFollowersForUser;

-(void)getFollowingForUser;

-(void)getUserWithObjectId:(NSString *)objectId;

@end
