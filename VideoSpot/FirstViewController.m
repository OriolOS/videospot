//
//  FirstViewController.m
//  VideoSpot
//
//  Created by Winparf on 15/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <ParseUI/ParseUI.h>
#import "FirstViewController.h"
#import "VideoTableViewCell.h"
#import "AdvertisementTableViewCell.h"

@interface FirstViewController () <UITableViewDataSource, UITableViewDelegate,PFLogInViewControllerDelegate,PFSignUpViewControllerDelegate, VideoPostServiceManagerDelegate, UserServiceManagerDelegate, FollowersServiceManagerDelegate>

@property(nonatomic,strong) PFLogInViewController *logInViewController;
@property(nonatomic,strong) PFLogInView *logInView;
@property(nonatomic,strong) PFSignUpViewController *signUpViewController;

@property(nonatomic,strong) UIRefreshControl *refreshControl;
@property(nonatomic,strong) NSString *filterPost;
@property(nonatomic) NSInteger skip;
@property(nonatomic) NSInteger lastRowDisplayed;


@property(nonatomic,strong) NSMutableArray *videoPosts;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.videoPosts = [NSMutableArray new];
    self.videoPostServiceManager = [[VideoPostServiceManager alloc]init];
    self.videoPostServiceManager.delegate = self;
    
    self.followersServiceManager = [[FollowersServiceManager alloc]init];
    self.followersServiceManager.delegate = self;
    
    self.userManager = [[UserServiceManager alloc]init];
    self.userManager.delegate = self;
    
    self.skip = 0;
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor lightGrayColor];
    self.refreshControl.tintColor = [UIColor darkGrayColor];
    [self.refreshControl addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    self.refreshControl.attributedTitle = attributedTitle;
    
    [self.tableView addSubview:self.refreshControl];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didReceivedInvalidSession) name:@"invalidSessionHandling" object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]) {
        if (!_logInViewController) {
            
            _logInViewController = [self resetLogInViewController];
        }
        [self presentViewController:self.logInViewController animated:YES completion:NULL];
    }else{
        [self.videoPostServiceManager getVideoPostWithFilter:@"" WithSkipOf:0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(PFLogInViewController *)resetLogInViewController{
    
    self.logInViewController = nil;
    
    PFLogInViewController *logInVC = [[PFLogInViewController alloc]init];
    
    UIImageView *logo1 = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/6, 20, self.view.frame.size.width * 2/3, self.view.frame.size.height *1/4)];
    logo1.contentMode = UIViewContentModeScaleAspectFit;
    logo1.image = [UIImage imageNamed:@"LocationTestLogo"];
    UIImageView *logo2 = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/6, 20, self.view.frame.size.width * 2/3, self.view.frame.size.height *1/4)];
    logo2.contentMode = UIViewContentModeScaleAspectFit;
    logo2.image = [UIImage imageNamed:@"LocationTestLogo"];
    
    logInVC.fields = (PFLogInFieldsUsernameAndPassword
                      | PFLogInFieldsLogInButton
                      | PFLogInFieldsPasswordForgotten
                      | PFLogInFieldsSignUpButton
                      | PFLogInFieldsDismissButton);
    //    //Set event for facebook button
    [logInVC.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]]];
    [logInVC.logInView.logo setContentMode:UIViewContentModeScaleAspectFit];
    [logInVC.logInView addSubview:logo1];
    [logInVC setDelegate:self];
    self.signUpViewController = [[PFSignUpViewController alloc]init];
    [self.signUpViewController setDelegate:self];
    [logInVC setSignUpController:self.signUpViewController];
    [logInVC.signUpController.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]]];
    [logInVC.signUpController.signUpView.logo setContentMode:UIViewContentModeScaleAspectFit];
    [logInVC.signUpController.signUpView addSubview:logo2];
    logInVC.signUpController.delegate = self;
    
    return logInVC;
}

-(void)didReceivedInvalidSession{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserApp"];
    
    self.logInViewController = [self resetLogInViewController];
    [self presentViewController:self.logInViewController animated:YES completion:nil];
    
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    
}

- (IBAction)searchButtonPressed:(id)sender {
    
    [self.videoPosts removeAllObjects];
    self.filterPost = self.searchTextField.text;
    self.skip = 0;
    [self.videoPostServiceManager getVideoPostWithFilter:self.filterPost WithSkipOf:0];
    self.editing = NO;
}

-(void)refreshTableView{
    [self.videoPosts removeAllObjects];
    self.skip = 0;
    if (self.searchTextField.text.length) {
        
        self.filterPost = self.searchTextField.text;
        [self.videoPostServiceManager getVideoPostWithFilter:self.filterPost WithSkipOf:0];
    }else{
        [self.videoPostServiceManager getVideoPostWithFilter:@"" WithSkipOf:0];
    }
}

- (void)userLabelTouchUpInside:(id)sender{
    
    UIButton *usernameButton = (UIButton *)sender;
    VideoPost *videoPost = [self.videoPosts objectAtIndex:usernameButton.tag];
    PFUser *currentUser = [PFUser currentUser];
    
    if (![currentUser.username isEqualToString:videoPost.username]) {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Do you want to follow this user?"
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction *action)
                             {
                                 
                                 [self.userManager getUserWithUserName:videoPost.username];
                                 
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                             }];
        
        UIAlertAction *cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction *action)
                                 {
                                     [alertController dismissViewControllerAnimated:YES completion:nil];
                                 }];
        [alertController addAction:cancel];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"This user is yourself"
                                                              message:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"CANCEL"
                                                    otherButtonTitles: nil];
        [myAlertView show];
        
    }
}

-(void) playButtonPressed:(id)sender{
    
    UIButton *playButton = (UIButton *)sender;
    VideoPost *videoPost = [self.videoPosts objectAtIndex:playButton.tag];
    
    // save it to the documents directory
    NSURL *fileURL = [self grabFileURL:@"tmp.mov"];
    [videoPost.videoData writeToURL:fileURL atomically:YES];
    
    // save it to the Camera Roll
    UISaveVideoAtPathToSavedPhotosAlbum([NSString stringWithFormat:@"%@",fileURL], nil, nil, nil);
    
    PlayerViewController *playerVC = [[PlayerViewController alloc]initWithContentURL:fileURL];
    [self presentMoviePlayerViewControllerAnimated:playerVC];
    
}

- (NSURL*)grabFileURL:(NSString *)fileName {
    
    // find Documents directory
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    // append a file name to it
    documentsURL = [documentsURL URLByAppendingPathComponent:fileName];
    
    return documentsURL;
}

-(NSString *)getTimeAgoForPostDate:(NSDate *)date{
    NSString *dateStr = @"";
    
    // get current date/time
    NSDate *today = [NSDate date];
    
    NSTimeInterval interval = [today timeIntervalSinceDate:date];
    
    NSNumber *intervalNumber = [NSNumber numberWithDouble:interval];
    float sec = [intervalNumber floatValue];
    
    float hour = sec/3600;
    int hourInt = hour;
    
    float minutes = sec/60 - hourInt*60;
    int minutesInt = minutes;
    
    int secInt = sec - hourInt*3600 - minutesInt*60;
    
    if (hour >= 24) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-mm-yyyy"];
        //Optionally for time zone conversions
        //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
        dateStr = [formatter stringFromDate:date];
    }else if (hourInt != 0 ){
        if (minutesInt != 0) {
            dateStr = [NSString stringWithFormat:@"%d h %d min ago",hourInt,minutesInt];
        }else{
            dateStr = [NSString stringWithFormat:@"%d h ago",hourInt];
        }
    }else if (minutesInt != 0){
        if (secInt != 0) {
            dateStr = [NSString stringWithFormat:@"%d min %d sec ago",minutesInt,secInt];
        }else{
            dateStr = [NSString stringWithFormat:@"%d min ago",minutesInt];
        }
    }else if (secInt >= 5){
        dateStr = [NSString stringWithFormat:@"%d sec ago",secInt];
    }else{
        dateStr = [NSString stringWithFormat:@"Now"];
    }
    
    return dateStr;
}


#pragma mark - UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.videoPosts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VideoTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"videoCell"];
    
    if (self.videoPosts.count) {
        VideoPost *videoPost = [self.videoPosts objectAtIndex:indexPath.row];
        
        cell.videoData = videoPost.videoData;
        
        cell.videoImageView.image = videoPost.previewVideoImage;
        cell.videoImageView.layer.cornerRadius = 10;
        cell.videoImageView.layer.masksToBounds = YES;
        
        cell.playButton.contentMode = UIViewContentModeScaleAspectFill;
        [cell.playButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.playButton setTag:indexPath.row];
        cell.playButton.layer.cornerRadius = 25;
        cell.playButton.layer.masksToBounds = YES;
        
        cell.userImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:videoPost.userImageUrl]]];
        cell.userImageView.layer.cornerRadius = 30;
        cell.userImageView.layer.masksToBounds = YES;
        
        
        [cell.usernameButton setTitle:videoPost.username forState:UIControlStateNormal];
        [cell.usernameButton addTarget:self action:@selector(userLabelTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [cell.usernameButton setTag:indexPath.row];
        
        cell.descriptionLabel.text = videoPost.descriptionVideo;
        cell.descriptionLabel.layer.cornerRadius = 10;
        cell.descriptionLabel.layer.masksToBounds = YES;
        
        NSString *tags = @"";
        if (videoPost.tagsArray.count) {
            for (NSString *tag in videoPost.tagsArray) {
                tags = [NSString stringWithFormat:@"%@%@",tags,tag];
            }
        }
        cell.tagsLabel.text = tags;
        
        cell.timeAgoLabel.text = [self getTimeAgoForPostDate:videoPost.createDate];
        
        return cell;

    }else{
        [self refreshTableView];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 200;
}

#pragma mark -UITableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 5 && self.skip != 0) {
        [self.videoPostServiceManager getVideoPostWithFilter:@"" WithSkipOf:self.skip];
    }else if (indexPath.row == self.skip - 5 && (self.skip%10 - ((int)self.skip%10)) == 0){
        [self.videoPostServiceManager getVideoPostWithFilter:@"" WithSkipOf:self.skip];
    }
}


#pragma mark - VideoPostServiceManager Delegate

-(void)didGetVideoPost:(NSArray *)videoPosts{
    if (videoPosts.count == 10) {
        self.skip += 10;
    }else{
        self.skip = 0;
    }
    
    for (PFObject *object in videoPosts) {
        
        PFFile * file;
        
        VideoPost *videoPost = [VideoPost new];
        videoPost.tagsArray = [[NSMutableArray alloc]init];
        videoPost.descriptionVideo = object[@"Description"];
        [videoPost.tagsArray addObjectsFromArray:object[@"Tags"]];
        
        videoPost.username = object[@"Owner"];
        
        file = object[@"OwnerImage"];
        videoPost.userImageUrl = file.url;
        
        file = object[@"PreviewImage"];
        videoPost.previewVideoImage = [UIImage imageWithData:[file getData]];
        
        file = object[@"Video"];
        videoPost.videoData = [file getData];
        
        videoPost.createDate = object.createdAt;
        
        [self.videoPosts addObject:videoPost];
        
    }
       
    [self.tableView reloadData];
    
    [self.refreshControl endRefreshing];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm:ss a"];
    NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
    self.refreshControl.attributedTitle = attributedTitle;
}

#pragma mark - FollowersServiceManager Delegate

-(void)didJustFollowUser:(BOOL)succeeded{
    if (succeeded) {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"You are following this user"
                                                              message:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Could not finish this operation"
                                                              message:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"CANCEL"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }

}

-(void)alreadyFollowing{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"You are already following this user"
                                                          message:nil
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles: nil];
    [myAlertView show];
}


#pragma mark - UserServiceManager Delegate

-(void)didReceivedUserByName:(PFObject *)user{
    
    if (user) {
        [self.followersServiceManager followUser:user.objectId];
    }else{
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"This user does not exist"
                                                              message:nil
                                                             delegate:nil
                                                    cancelButtonTitle:@"CANCEL"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
}

#pragma mark PFLogInViewController Delegate

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user{
    
    PFFile *imageFile = user[@"ProfileImage"];
    NSString *imageUrl = [NSString stringWithFormat:@"%@", imageFile.url];

    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *userDict = @{
                               @"objectId" : user.objectId,
                               @"ProfileImage" : imageUrl,
                               @"username" : user.username,
                               @"email" : user.email};
    
    [defaults setObject:userDict forKey:@"UserApp"];
    [defaults synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)signUpViewController:(PFSignUpViewController * __nonnull)signUpController didSignUpUser:(PFUser * __nonnull)user{

    PFFile *imageFile = user[@"ProfileImage"];
    NSString *imageUrl = [NSString stringWithFormat:@"%@", imageFile.url];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *userDict = @{
                               @"objectId" : user.objectId,
                               @"ProfileImage" : imageUrl,
                               @"username" : user.username,
                               @"email" : user.email};
    
    [defaults setObject:userDict forKey:@"UserApp"];
    [defaults synchronize];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
