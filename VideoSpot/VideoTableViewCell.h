//
//  VideoPublishTableViewCell.h
//  VideoSpot
//
//  Created by Winparf on 15/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIButton *usernameButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *timeAgoLabel;

@property (nonatomic, strong) NSString *urlVideo;
@property (nonatomic, strong) NSData *videoData;

@end
