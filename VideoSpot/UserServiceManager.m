//
//  UserServiceManager.m
//  VideoSpot
//
//  Created by Winparf on 20/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "ParseErrorHandlingController.h"
#import "UserServiceManager.h"

@implementation UserServiceManager


- (void)updateUserWithUsername:(NSString *)username Email:(NSString *)email{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        
        PFACL *acl=[PFACL ACL];
        [acl setPublicReadAccess:YES];
        [acl setPublicWriteAccess:YES];

        
        PFUser *currentUser = [PFUser currentUser];
        PFFile *imageFile;
        NSString *userImage;

        userImage = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]objectForKey:@"imageProfile"];
       
        if ([userImage isKindOfClass:[UIImage class]]) {
            NSData *imageData = UIImageJPEGRepresentation((UIImage *)userImage, 0);
            NSString *filename = @"imageProfile";
            imageFile = [PFFile fileWithName:filename data:imageData];
            [currentUser setObject:imageFile forKey:@"image"];
        }else if (userImage && ![userImage isEqualToString:@"(null)"]){
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userImage]];
            NSString *filename = @"imageProfile";
            imageFile = [PFFile fileWithName:filename data:imageData];
        }else{
            NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"LocationTestLogo"], 0);
            NSString *filename = @"imageProfile";
            imageFile = [PFFile fileWithName:filename data:imageData];
        }
        
        [currentUser setObject:imageFile forKey:@"image"];
        [currentUser setObject:username forKey:@"username"];
        [currentUser setObject:email forKey:@"email"];
        
        
        [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                // The currentUser saved successfully.
                NSLog(@"Update user succeeded");
                [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]setObject:username forKey:@"username"];
                [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserApp"]setObject:email forKey:@"email"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
              
            } else {
                // There was an error saving the currentUser.
                [ParseErrorHandlingController handleParseError:error];
            }
            [self.delegate didUpdateUser:succeeded];
        }];
    }else{
        [self.delegate hasNoConnection];
    }
}

-(void)setImageProfile:(UIImage *) image{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        PFQuery *query = [PFUser query];
        [query whereKey:@"objectId" equalTo:currentUser.objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
            if(!error){
                
                PFFile *imageFile;
            
                NSData *imageData = UIImageJPEGRepresentation(image, 0);
                NSString *filename = @"imageProfile";
                imageFile = [PFFile fileWithName:filename data:imageData];
                
                
                [user setObject:imageFile forKey:@"ProfileImage"];
                
                [user pinInBackground];
                
                [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    if (succeeded) {
                        
                        [user unpinInBackground];
                        [self.delegate didSetImage:imageFile.url];
                        
                    }else{
                        [ParseErrorHandlingController handleParseError:error];
                    }
                    
                }];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)getUserWithId:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"objectId" equalTo:objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
            if(!error){
                
                [self.delegate didReceivedUserById:user];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)getUserWithUserName:(NSString *)username{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"username" equalTo:username];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
            if(!error){
                
                [self.delegate didReceivedUserByName:user];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}


@end
