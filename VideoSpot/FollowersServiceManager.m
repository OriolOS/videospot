//
//  FollowersServiceManager.m
//  VideoSpot
//
//  Created by Winparf on 22/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "ParseErrorHandlingController.h"
#import "FollowersServiceManager.h"

@implementation FollowersServiceManager

-(void)getFollowingForUser{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        PFQuery *query = [PFQuery queryWithClassName:@"Followers"];
        [query whereKey:@"Follower" equalTo:currentUser.objectId];
        [query findObjectsInBackgroundWithBlock:^(NSArray *result, NSError *error) {
            if(!error){
                
                [self.delegate didReceivedFollowing:result];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)getFollowersForUser{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        PFQuery *query = [PFQuery queryWithClassName:@"Followers"];
        [query whereKey:@"Following" equalTo:currentUser.objectId];
        [query findObjectsInBackgroundWithBlock:^(NSArray *result, NSError *error) {
            if(!error){
                
                [self.delegate didReceivedFollowers:result];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)followUser:(NSString *)objectId{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFUser *currentUser = [PFUser currentUser];
        
        PFQuery *query = [PFQuery queryWithClassName:@"Followers"];
        [query whereKey:@"Follower" equalTo:currentUser.objectId];
        [query whereKey:@"Following" equalTo:objectId];
        [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
            
            if (number == 0) {
                PFObject *object = [PFObject objectWithClassName:@"Followers"];
                object[@"Follower"] = currentUser.objectId;
                object[@"Following"] = objectId;
                
                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if(!error){
                        
                        [self.delegate didJustFollowUser:succeeded];
                        
                    }else{
                        [ParseErrorHandlingController handleParseError:error];
                    }
                }];

            }else if (number > 0){
                [self.delegate alreadyFollowing];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
            
        }];
        
        
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)getUserWithObjectId:(NSString *)objectId{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        PFQuery *query = [PFUser query];
        [query whereKey:@"objectId" equalTo:objectId];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
            if(!error){
                
                [self.delegate didReceivedUserByObjectId:user];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }
}
@end
