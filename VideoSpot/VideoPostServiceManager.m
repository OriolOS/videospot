//
//  VideoPostServiceManager.m
//  VideoSpot
//
//  Created by Winparf on 20/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import "VideoPostServiceManager.h"

@implementation VideoPostServiceManager

-(void)saveVideoPost:(VideoPost *)videoPost{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        // Create a PFObject using the Post class and set the values we extracted above
        PFObject *postObject = [PFObject objectWithClassName:@"VideoPost"];
        
        PFFile *userProfileImage;
        if (videoPost.userImageUrl.length) {
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:videoPost.userImageUrl]];
            NSString *filename = @"userProfileImage";
            if (imageData != nil) {
                userProfileImage = [PFFile fileWithName:filename data:imageData];
                postObject[@"OwnerImage"] = userProfileImage;
            }
        }
        
        PFFile *videoPreviewImage;
        if (videoPost.previewVideoImage) {
            NSData *imageData = UIImageJPEGRepresentation(videoPost.previewVideoImage, 0);
            NSString *filename = @"videoPreviewImage";
            videoPreviewImage = [PFFile fileWithName:filename data:imageData];
            postObject[@"PreviewImage"] = videoPreviewImage;
        }
        
        PFFile *video;
        if (videoPost.urlVideo) {
            NSData *videoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:videoPost.urlVideo]];
            NSString *filename = @"Video";
            video = [PFFile fileWithName:filename data:videoData];
            postObject[@"Video"] = video;
        }

        
        
        PFACL *acl=[PFACL ACL];
        [acl setPublicReadAccess:YES];
        
        postObject[@"Description"] = videoPost.descriptionVideo;
        postObject[@"Tags"] = [videoPost.tagsArray copy];
        postObject[@"Owner"] = videoPost.username;
        postObject.ACL = acl;
        
        [postObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {  // Failed to save, show an alert view with the error message
                
                [ParseErrorHandlingController handleParseError:error];
            }
            if (succeeded) {  // Successfully saved, post a notification to tell other view controllers
                
                NSLog(@"Save succeeded");
            } else {
                NSLog(@"Failed to save.");
            }
            [self.delegate didSavedVideoPost:succeeded];
        }];
    }else{
        [self.delegate hasNoConnection];
        
    }

}

-(void)getVideoPostWithFilter:(NSString *)filter WithSkipOf:(NSInteger)skip{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        NSMutableArray *filterArray = [NSMutableArray new];
        [filterArray addObject:filter];
        
        NSPredicate *predicate;
        if ([filter containsString:@"#"]) {
            predicate = [NSPredicate predicateWithFormat:@"Tags IN %@", filterArray];
        }else if(filter.length){
            predicate = [NSPredicate predicateWithFormat:@"Owner = %@", filter];
        }else{
            predicate = nil;
        }
        
        PFQuery *query = [PFQuery queryWithClassName:@"VideoPost" predicate:predicate];
        [query orderByDescending:@"createdAt"];
        query.limit = 10;
        query.skip = skip;
        [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
            if(!error){
                [self.delegate didGetVideoPost:results];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
            
        }];

        
    }else{
        [self.delegate hasNoConnection];
        
    }
}

-(void)getVideoPostWithFollowingArray:(NSArray *)filter WithSkipOf:(NSInteger)skip{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        NSMutableArray *filterArray = [NSMutableArray new];
        PFUser *currentUser = [PFUser currentUser];
        if (filter.count) {
            [filterArray addObjectsFromArray:filter];
        }
        [filterArray addObject:currentUser.username];
    
        PFQuery *query = [PFQuery queryWithClassName:@"VideoPost"];
        
        [query whereKey:@"Owner" containedIn:filterArray];
        [query orderByDescending:@"createdAt"];
        query.limit = 10;
        query.skip = skip;
        [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
            if(!error){
                [self.delegate didGetVideoPost:results];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
            
        }];
        
        
    }else{
        [self.delegate hasNoConnection];
        
    }
}


-(void)getCountOfVideoPost{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {

        PFUser *user = [PFUser currentUser];
        
        PFQuery *query = [PFQuery queryWithClassName:@"VideoPost"];
        [query whereKey:@"Owner" equalTo:user.username];
        [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
            if(!error){
                [self.delegate didGetNumberOfPost:number];
            }else{
                [ParseErrorHandlingController handleParseError:error];
            }
            
        }];
        
        
    }else{
        [self.delegate hasNoConnection];
        
    }
}


@end
