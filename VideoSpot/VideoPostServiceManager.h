//
//  VideoPostServiceManager.h
//  VideoSpot
//
//  Created by Winparf on 20/7/15.
//  Copyright (c) 2015 OOS. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "VideoPost.h"
#import <Parse/Parse.h>
#import "Reachability.h"
#import "ParseErrorHandlingController.h"

@protocol VideoPostServiceManagerDelegate

-(void) didSavedVideoPost:(BOOL)succeeded;

-(void) didGetVideoPost:(NSArray *)videoPosts;

-(void) didGetNumberOfPost:(int)count;

//Reachability method delegate

-(void)hasNoConnection;

@end

@interface VideoPostServiceManager : NSObject

@property (nonatomic,strong) id <VideoPostServiceManagerDelegate> delegate;

-(void)saveVideoPost:(VideoPost *)videoPost;

-(void)getVideoPostWithFilter:(NSString *)filter WithSkipOf:(NSInteger)skip;

-(void)getVideoPostWithFollowingArray:(NSArray *)filter WithSkipOf:(NSInteger)skip;

-(void)getCountOfVideoPost;

@end
